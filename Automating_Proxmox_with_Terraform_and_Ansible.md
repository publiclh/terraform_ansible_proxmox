# Create LXC on Proxmux with Terraform and install WebServer, docker and Docker-Compose with Ansible
## 1. Customize AlmaLinux9 LXC Container with Proxmox
##### * Note: The AlmaLinux9-based container template provided by Proxmox doesn’t have SSH installed, which makes it fairly useless for Ansible-based deployments.
## Steps to update and reroll a template:
## 1.1 Create and connect to the container.
```
root@pve2:~# pct create 111 local:vztmpl/almalinux-9-default_20221108_amd64.tar.xz --storage local-lvm --net0 name=eth0,ip=dhcp,ip6=dhcp,bridge=vmbr0 --password=SetPassOfTemplateLXC
root@pve2:~# pct start 111
root@pve2:~# pct console 111
```
## 1.2 Install openssh in template AlmaLinux9 to enable ssh connection for Ansible.
```
[root@CT111 ~]# dnf update -y
[root@CT111 ~]# dnf install epel-release -y
[root@CT111 ~]# yum install openssh -y
[root@CT111 ~]# yum install openssh-server -y
[root@CT111 ~]# systemctl start sshd
[root@CT111 ~]# systemctl restart sshd
// Modify `sshd_config`
[root@CT111 ~]# sed -ri 's/PermitEmptyPasswords no/PermitEmptyPasswords yes/' /etc/ssh/sshd_config
[root@CT111 ~]# sed -ri 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
```
## 1.3 PowerOff the modified template.
```
[root@CT111 ~]# poweroff
```
## 1.4 Create new modified template with openssh installed
```
root@pve2:~# vzdump 111 --compress gzip --dumpdir /root/
root@pve2:~# mv vzdump-lxc-111-2022_12_21-13_56_21.tar.gz /var/lib/vz/template/cache/almalinux-9-ssh-default_20221108_amd64_Ledi.tar.gz
root@pve2:~# ls /var/lib/vz/template/cache/
almalinux-9-default_20221108_amd64.tar.xz  almalinux-9-ssh-default_20221108_amd64_Ledi.tar.gz
root@pve2:~# cp /var/lib/vz/template/cache/almalinux-9-ssh-default_20221108_amd64_Ledi.tar.gz /var/lib/vz/template/cache/almalinux-9-ssh-default_20221108_amd64_Ledi.tar.xz
```
## 2. Create LXC with Terrraform on Proxmox
### 2.1 Create main.tf file

```
terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = "2.7.4"
    }
  }
}

provider "proxmox" {
    
  pm_api_url = "https://192.168.1.162:8006/api2/json"

  pm_api_token_id = "root@pam!TerraformTokenLedi"

  pm_api_token_secret = "03624a3e-eec5-43ac-be35-482ac5d451d7"

  pm_tls_insecure = true
}

resource "proxmox_lxc" "lxc-test" { 
    target_node = "pve2"   # node of your cluster on which deployment should be done
    cores = 1
    memory = "1024"
    swap = "1024"
    hostname = "CT-SecTemplate"
    password = "Pass_of_LXC"
    network {
        name = "eth0"
        bridge = "vmbr0"
        ip = "192.168.1.167/24"
        #ip = "dhcp"
        gw = "192.168.1.1"
        firewall = true
        #nameserver = "192.168.1.200"
        #mtu = "1450"		
        
    }
    ostemplate = "local:vztmpl/almalinux-9-ssh-default_20221108_amd64_Ledi.tar.gz"
    rootfs {
      storage = "Ceph-pool1"
      size = "3G"
    }
    start = true # start after creation
    ssh_public_keys = <<-EOT
      ssh-rsa AAAAB3NzaC1yc2EAAAADAQAB------------------------PUBLIC-KEY-OF-ANSIBLE-SERVER----------LH------------------------------------------------------------------------------------= ansible
    EOT
}
```
### 2.2 Run Terraform commands to create LXC 
```
# terraform init
# terraform plan
# terraform apply
# terraform state rm proxmox_lxc.lxc-test
```
## 3. Install apache, docker and docker-compose with Ansible on LXC created by Terraform
### 3.1 On same dir create two file, hosts and ansibleexample.yaml
root@ledi:/home/projects/ansible/example1# cat hosts 
```
[ansible_client]
192.168.1.167

[ansible_clients]
192.168.1.166
192.168.1.167
```
root@ledi:/home/projects/ansible/example1# cat ansibleexample.yaml
```
---
- name: Ansible Playbook to Install and Setup Apache on Ubuntu
  hosts: ansible_client
  become: yes
  tasks:
    - name: httpd installed
      ansible.builtin.yum:
        name: httpd
        state: latest
    - name: custom index.html
      ansible.builtin.copy:
        dest: /var/www/html/index.html
        content: |
                    ARKIT Ansible Automation by LH
    - name: httpd service enabled
      ansible.builtin.service:
        name: httpd
        enabled: true
        state: started
    - name: Set up docker-ce repository
      shell: |
        sudo dnf update -y
        sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
        sudo dnf makecache
        sudo dnf install docker-ce --nobest -y
        sudo systemctl start docker
        sudo systemctl enable docker
      args:
        warn: no
    - name: Set up docker-compose repository
      shell: |
        sudo dnf install curl
        sudo dnf install libxcrypt-compat -y
        sudo curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-`uname  -s`-`uname -m` -o /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose
      args:
        warn: no
```
### 3.2 Run Ansible commands to install webserver, docker and docker-compose on LXC
```
root@ledi:/home/projects/ansible/example1# ansible-playbook -i hosts ansibleexample.yaml
```
[Ledion Haderi](https://al.linkedin.com/in/ledion-haderi-00473968) :thumbsup:
