# Instalimi i DRBD Version 9 ne AlmaLinux Version 8.7 
## Skema e lidhjes se serverave 
![screenshot-create-blank-project](/images/images_DRBD-Schema.png)

#### 1 Konfigurimi i hostname dhe hosts ne te dy serverat mirror1 dhe mirror2
```
[root@mirror1 ~]# cat /etc/hostname
mirror1
[root@mirror1 ~]# cat /etc/hosts
192.168.121.51 mirror2
192.168.121.50 mirror1
```

```
[root@mirror2 ~]# cat /etc/hostname
mirror2
[root@mirror2 ~]# cat /etc/hosts
192.168.121.51 mirror2
192.168.121.50 mirror1

```
#### 2 Konfigurimi i network 
```
[root@mirror1 ~]# cat /etc/sysconfig/network-scripts/ifcfg-eno3
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=none
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=no
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=eui64
NAME=eno3
UUID=507368c2-16e5-467d-a9f7-eef0a52722c3
DEVICE=eno3
ONBOOT=yes
IPADDR=192.168.1.50
PREFIX=24
GATEWAY=192.168.1.1
DNS1=192.168.1.200
DNS2=192.168.1.201
IPV6_DISABLED=yes

[root@mirror1 ~]# cat /etc/sysconfig/network-scripts/ifcfg-eno4
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=none
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=no
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=eui64
NAME=eno4
UUID=f86f02d9-9c25-4455-8987-9f75d2445b6c
DEVICE=eno4
ONBOOT=yes
IPADDR=192.168.121.50
PREFIX=24
#GATEWAY=192.168.121.1
#DNS1=8.8.8.8
#DNS2=8.8.4.4
IPV6_DISABLED=yes
```

```
[root@mirror2 ~]# cat /etc/sysconfig/network-scripts/ifcfg-eno3
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=none
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=no
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=eui64
NAME=eno3
UUID=4a12165a-9dd2-4738-b851-764d6a25273b
DEVICE=eno3
ONBOOT=yes
IPADDR=192.168.1.51
PREFIX=24
GATEWAY=192.168.1.1
DNS1=192.168.1.200
DNS2=192.168.1.201
IPV6_DISABLED=yes

[root@mirror2 ~]# cat /etc/sysconfig/network-scripts/ifcfg-eno
ifcfg-eno1  ifcfg-eno2  ifcfg-eno3  ifcfg-eno4
[root@mirror2 ~]# cat /etc/sysconfig/network-scripts/ifcfg-eno4
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=none
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=no
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=eui64
NAME=eno4
UUID=19ccd8ae-b5e6-4e1d-81b4-7395a8bf6971
DEVICE=eno4
ONBOOT=yes
IPADDR=192.168.121.51
PREFIX=24
#GATEWAY=192.168.121.1
#DNS1=8.8.8.8
#DNS2=8.8.4.4
IPV6_DISABLED=yes

```

#### 3 Configure SSH Key Based Authentication for ssh access for Mirror1 and Mirror2
##### 3.1 Generate ssh keys on mirror1 and mirror2
```
[root@mirror1 ~]# ssh-keygen -t rsa

[root@mirror1 ~]# ssh-copy-id -i /root/.ssh/id_rsa.pub root@192.168.1.51

[root@mirror1 ~]# cat ~/.ssh/authorized_keys
ssh-rsa AAA......= generatedfrommirror2
```
```
[root@mirror2 ~]# ssh-keygen -t rsa

[root@mirror2 ~]# ssh-copy-id -i /root/.ssh/id_rsa.pub root@192.168.1.50

root@mirror2 ~]# cat ~/.ssh/authorized_keys
ssh-rsa AAAA........= generatedfrommirror1

```

#### 4 Install DRBD and PacemakerCoroSync

Install DRBD, the DRBD kernel module, and the Pacemaker configuration system.
```
dnf install drbd90-utils kmod-drbd90 pcs
```
##### 4.1 Configure DRBD
After installed DRBD, we’ll need to edit /etc/drbd.conf and create our resource configuration file /etc/drbd.d/drbdlh1.res with the following contents:
* Mirror1
```
[root@mirror1 ~]# cat /etc/drbd.conf
include "drbd.d/*.res";

[root@mirror1 ~]# cat /etc/drbd.d/drbdlh1.res
resource drbdlh1 {
  protocol C;
  device /dev/drbd1;
  disk /dev/sdb;
  meta-disk internal;
  on mirror1 {
  address 192.168.121.50:7788;
  }
  on mirror2 {
  address 192.168.121.51:7788;
  }
}
```
* Mirror2
```
[root@mirror2 ~]# cat /etc/drbd.conf
include "drbd.d/*.res";
[root@mirror2 ~]# cat /etc/drbd.d/drbdlh1.res
resource drbdlh1 {
  protocol C;
  device /dev/drbd1;
  disk /dev/sdb;
  meta-disk internal;
  on mirror1 {
  address 192.168.121.50:7788;
  }
  on mirror2 {
  address 192.168.121.51:7788;
  }
}
```
#### 4.2 Create the resource metadata
```
[root@mirror1 drbd.d]# drbdadm create-md drbd1lh          //on both site
initializing activity log
initializing bitmap (157 MB) to all zero
Writing meta data...
New drbd meta data block successfully created.
[root@mirror2 drbd.d]#drbdadm create-md drbd1lh           //on both site
New drbd meta data block successfully created.
//Bring the device up on both nodes and verify their states by entering the following commands
[root@mirror1 drbd.d]# drbdadm up drbd1lh                //on both site
[root@mirror2 drbd.d]# drbdadm up drbd1lh                //on both site
[root@mirror1 drbd.d]# drbdadm primary --force drbd1lh
[root@mirror1 drbd.d]# drbdadm status drbd1lh
drbdlh1 role:Primary
  disk:UpToDate
  mirror2 role:Secondary
    peer-disk:UpToDate

```
#### 4.3 Create a File System on DRBD Backed Device

After DRBD is initialized, we’ll need to make 'mirror1' Primary and create a file system on the
DRBD device that we configured earlier as resource drbd1lh.
First, make the mirror1 Primary for resource drbd1lh by entering the following command on
mirror1:
```
[root@mirror1 ~]# drbdadm primary drbdlh               ///vetem tek primari
[root@mirror1 ~]# drbdadm primary --force drbdlh       ///vetem tek primari
//Next, on mirror1 and mirror2, create a file system on the backing device.
[root@mirror1 drbd.d]# mkfs.xfs /dev/drbd1                       //on both site  
//Create the following directory on both 'node-a' and 'node-b':      
[root@mirror1 drbd.d]# cd /
[root@mirror1 /]# mkdir homedata                                   //on both site
[root@mirror1 /]# mount /dev/drbd1 /homedata/                     //on both site 
```

#### 5 Install and configuration Pacemaker and Corosync

##### 5.1 Install Pacemaker and Corosync
```
dnf install pacemaker corosync
systemctl enable pacemaker corosnc
systemctl start pacemaker corosnc
nano /etc/corosunc/corosync.conf 
```

##### 5.2 Configuration Pacemaker and Corosync
* Mirror1
```
[root@mirror1 ~]# cat /etc/corosync/corosync.conf
totem {
  version: 2
  secauth: off
  cluster_name: cluster
  transport: knet
  rrp_mode: passive
}
nodelist {
  node {
  ring0_addr: 192.168.121.50
  ring1_addr: 192.168.1.50
  nodeid: 1
  name: mirror1
  }
  node {
  ring0_addr: 192.168.121.51
  ring1_addr: 192.168.1.51
  nodeid: 2
  name: mirror2
  }
}
quorum {
  provider: corosync_votequorum
  two_node: 1
}
logging {
  to_syslog: yes
} 
```
* Mirror2
```
[root@mirror2 ~]# cat /etc/corosync/corosync.conf
totem {
  version: 2
  secauth: off
  cluster_name: cluster
  transport: knet
  rrp_mode: passive
}
nodelist {
  node {
  ring0_addr: 192.168.121.50
  ring1_addr: 192.168.1.50
  nodeid: 1
  name: mirror1
  }
  node {
  ring0_addr: 192.168.121.51
  ring1_addr: 192.168.1.51
  nodeid: 2
  name: mirror2
  }
}
quorum {
  provider: corosync_votequorum
  two_node: 1
}
logging {
  to_syslog: yes
}
```
* Check status of DRBD
```
[root@mirror1 home]# drbdadm status
drbdlh role:Primary
  disk:UpToDate
  mirror2 role:Secondary
    peer-disk:UpToDate
[root@mirror2 home]# drbdadm status
drbdlh role:Secondary
  disk:UpToDate
  mirror1 role:Primary
    peer-disk:UpToDate
```
Repeat the preceding Pacemaker and Corosync installation and configuration steps on each cluster node. Verify that
everything has been started and is working correctly by entering the following crm_mon command. You should get
output similar to this:	
```
[root@mirror2 home]# systemctl start corosync pacemaker

[root@mirror2 home]# crm_mon

[root@mirror2 home]# crm_mon -rf -n1

Status of pacemakerd: 'Pacemaker is running' (last updated 2023-03-01 15:19:26 +01:00)
Cluster Summary:
  * Stack: corosync
  * Current DC: mirror1 (version 2.1.4-5.el8_7.2-dc6eb4362e) - partition with quorum
  * Last updated: Wed Mar  1 15:19:26 2023
  * Last change:  Wed Mar  1 15:08:30 2023 by hacluster via crmd on mirror1
  * 2 nodes configured
  * 0 resource instances configured
Node List:
  * Node mirror1: online:
    * Resources:
  * Node mirror2: online:
    * Resources:
Inactive Resources:
  * No inactive resources
Migration Summary:

[root@mirror2 home]# sudo systemctl restart corosync

[root@mirror2 home]# sudo systemctl restart pacemaker

[root@mirror2 home]# pcs status
Since we only have two nodes, we will need to tell Pacemaker to ignore quorum. Run the following commands from either cluster node (but not both)
```
#### 5.3 Additional Cluster-Level Configuration
Since we only have two nodes, we will need to tell Pacemaker to ignore quorum. Run the following commands from either cluster node (but not both)
```
[root@mirror1 ~]# pcs property set no-quorum-policy=ignore               //only on node1
```
#### 5.4  Configure Pacemaker for HA. Configure DRBD Resources

Only on mirror1
```
# pcs cluster cib drbdconf
```
Only on mirror1
```
# pcs -f drbdconf resource create p_drbd_drbdlh1 ocf:linbit:drbd \
drbd_resource=drbdlh1 \
op start interval=0s timeout=240s \
stop interval=0s timeout=100s \
monitor interval=31s timeout=20s \
role=Unpromoted monitor interval=29s timeout=20s role=Promoted
```
Only on mirror1
```
# pcs -f drbdconf resource promotable p_drbd_drbdlh1 \
promoted-max=1 promoted-node-max=1 clone-max=2 clone-node-max=1 notify=true
```
Only on mirror1
```
# pcs cluster cib-push drbdconf
CIB updated
```
```
[root@mirror1 ~]# crm_mon
Cluster Summary:
  * Stack: corosync
  * Current DC: mirror1 (version 2.1.4-5.el8_7.2-dc6eb4362e) - partition with quorum
  * Last updated: Wed Mar  1 17:21:49 2023
  * Last change:  Wed Mar  1 17:20:28 2023 by root via cibadmin on mirror1
  * 2 nodes configured
  * 2 resource instances configured
Node List:
  * Online: [ mirror1 mirror2 ]
Active Resources:
  * Clone Set: p_drbd_drbdlh1-clone [p_drbd_drbdlh1] (promotable):
    * Masters: [ mirror1 ]
    * Slaves: [ mirror2 ]
[root@mirror2 ~]# crm_mon
Cluster Summary:
  * Stack: corosync
  * Current DC: mirror1 (version 2.1.4-5.el8_7.2-dc6eb4362e) - partition with quorum
  * Last updated: Wed Mar  1 17:20:42 2023
  * Last change:  Wed Mar  1 17:20:13 2023 by root via cibadmin on mirror1
  * 2 nodes configured
  * 2 resource instances configured
Node List:
  * Online: [ mirror1 mirror2 ]
Active Resources:
  * Clone Set: p_drbd_drbdlh1-clone [p_drbd_drbdlh1] (promotable):
    * Masters: [ mirror1 ]
    * Slaves: [ mirror2 ]
```
#### 5.5 Configure the File System Primitive 
```
pcs -f drbdconf resource create p_fs_drbd1 ocf:heartbeat:Filesystem \
device=/dev/drbd1 directory=/datahome fstype=xfs \
options=noatime,nodiratime \
op start interval="0" timeout="60s" \
stop interval="0" timeout="60s" \
monitor interval="20" timeout="40s"
```
```
pcs -f drbdconf constraint order promote p_drbd_drbdlh1-clone then start p_fs_drbd1
```
```
pcs -f drbdconf constraint colocation \
add p_fs_drbd1 with p_drbd_drbdlh1-clone INFINITY with-rsc-role=Promoted
```
```
pcs cluster cib-push drbdconf  
```                                                                            
```
crm_mon
```
#### 6 Configure DRBD Resources for dockerservice

Only on master node
```
pcs cluster cib drbdconf04042023
```
```
pcs -f drbdconf04042023 resource create docker-service systemd:docker \
op start interval="0" timeout="60s" \
stop interval="0" timeout="60s" \
monitor interval="20" timeout="40s"
```
```
pcs -f drbdconf04042023 constraint order promote p_drbd_drbdlh1-clone then start docker-service
```
```
pcs -f drbdconf04042023 constraint colocation \
add docker-service with p_drbd_drbdlh1-clone INFINITY with-rsc-role=Promoted
```
```
 pcs cluster cib-push drbdconf04042023
```
```
pcs resource restart dockerservice 
```
#### 7 Configure the Virtual IP Address
```
pcs cluster cib drbdconf04042023
pcs -f drbdconf04042023 resource create virtual_ip ocf:heartbeat:IPaddr2 \
ip=192.168.1.60 cidr_netmask=24 \
op monitor interval=20s timeout=20s \
start interval=0s timeout=20s \
stop interval=0s timeout=20s
```
```
pcs -f drbdconf04042023 constraint order promote p_drbd_drbdlh1-clone then start virtual_ip                       
```
```
pcs -f drbdconf04042023 constraint colocation \
add virtual_ip with p_drbd_drbdlh1-clone INFINITY with-rsc-role=Promoted
```
```
pcs cluster cib-push drbdconf04042023
```
```
crm_mon
```
#### Configuration files

```
[root@mirror1 ~]# cat drbdconf04042023
<cib crm_feature_set="3.15.0" validate-with="pacemaker-3.8" epoch="212" num_updates="0" admin_epoch="0" cib-last-written="Tue Apr  4 12:15:32 2023" update-origin="mirror1" update-client="cibadmin" update-user="root" have-quorum="1" dc-uuid="2">
  <configuration>
    <crm_config>
      <cluster_property_set id="cib-bootstrap-options">
        <nvpair id="cib-bootstrap-options-have-watchdog" name="have-watchdog" value="false"/>
        <nvpair id="cib-bootstrap-options-dc-version" name="dc-version" value="2.1.4-5.el8_7.2-dc6eb4362e"/>
        <nvpair id="cib-bootstrap-options-cluster-infrastructure" name="cluster-infrastructure" value="corosync"/>
        <nvpair id="cib-bootstrap-options-cluster-name" name="cluster-name" value="cluster"/>
        <nvpair id="cib-bootstrap-options-no-quorum-policy" name="no-quorum-policy" value="ignore"/>
        <nvpair id="cib-bootstrap-options-stonith-enabled" name="stonith-enabled" value="false"/>
        <nvpair id="cib-bootstrap-options-last-lrm-refresh" name="last-lrm-refresh" value="1679068116"/>
      </cluster_property_set>
    </crm_config>
    <nodes>
      <node id="1" uname="mirror1">
        <instance_attributes id="nodes-1"/>
      </node>
      <node id="2" uname="mirror2">
        <instance_attributes id="nodes-2"/>
      </node>
    </nodes>
    <resources>
      <clone id="p_drbd_drbdlh1-clone">
        <primitive class="ocf" id="p_drbd_drbdlh1" provider="linbit" type="drbd">
          <meta_attributes id="p_drbd_drbdlh1-meta_attributes"/>
          <instance_attributes id="p_drbd_drbdlh1-instance_attributes">
            <nvpair id="p_drbd_drbdlh1-instance_attributes-drbd_resource" name="drbd_resource" value="drbdlh1"/>
          </instance_attributes>
          <operations>
            <op id="p_drbd_drbdlh1-demote-interval-0s" interval="0s" name="demote" timeout="90"/>
            <op id="p_drbd_drbdlh1-monitor-interval-31s" interval="31s" name="monitor" role="Slave" timeout="20s"/>
            <op id="p_drbd_drbdlh1-monitor-interval-29s" interval="29s" name="monitor" role="Master" timeout="20s"/>
            <op id="p_drbd_drbdlh1-notify-interval-0s" interval="0s" name="notify" timeout="90"/>
            <op id="p_drbd_drbdlh1-promote-interval-0s" interval="0s" name="promote" timeout="90"/>
            <op id="p_drbd_drbdlh1-reload-interval-0s" interval="0s" name="reload" timeout="30"/>
            <op id="p_drbd_drbdlh1-start-interval-0s" interval="0s" name="start" timeout="240s"/>
            <op id="p_drbd_drbdlh1-stop-interval-0s" interval="0s" name="stop" timeout="100s"/>
          </operations>
        </primitive>
        <meta_attributes id="p_drbd_drbdlh1-clone-meta_attributes">
          <nvpair id="p_drbd_drbdlh1-clone-meta_attributes-clone-max" name="clone-max" value="2"/>
          <nvpair id="p_drbd_drbdlh1-clone-meta_attributes-clone-node-max" name="clone-node-max" value="1"/>
          <nvpair id="p_drbd_drbdlh1-clone-meta_attributes-notify" name="notify" value="true"/>
          <nvpair id="p_drbd_drbdlh1-clone-meta_attributes-promotable" name="promotable" value="true"/>
          <nvpair id="p_drbd_drbdlh1-clone-meta_attributes-promoted-max" name="promoted-max" value="1"/>
          <nvpair id="p_drbd_drbdlh1-clone-meta_attributes-promoted-node-max" name="promoted-node-max" value="1"/>
        </meta_attributes>
      </clone>
      <primitive class="ocf" id="p_fs_drbd1" provider="heartbeat" type="Filesystem">
        <instance_attributes id="p_fs_drbd1-instance_attributes">
          <nvpair id="p_fs_drbd1-instance_attributes-device" name="device" value="/dev/drbd1"/>
          <nvpair id="p_fs_drbd1-instance_attributes-directory" name="directory" value="/datahome"/>
          <nvpair id="p_fs_drbd1-instance_attributes-fstype" name="fstype" value="xfs"/>
          <nvpair id="p_fs_drbd1-instance_attributes-options" name="options" value="noatime,nodiratime"/>
        </instance_attributes>
        <operations>
          <op id="p_fs_drbd1-monitor-interval-20" interval="20" name="monitor" timeout="40s"/>
          <op id="p_fs_drbd1-start-interval-0" interval="0" name="start" timeout="60s"/>
          <op id="p_fs_drbd1-stop-interval-0" interval="0" name="stop" timeout="60s"/>
        </operations>
      </primitive>
      <primitive class="ocf" id="virtual_ip" provider="heartbeat" type="IPaddr2">
        <instance_attributes id="virtual_ip-instance_attributes">
          <nvpair id="virtual_ip-instance_attributes-cidr_netmask" name="cidr_netmask" value="24"/>
          <nvpair id="virtual_ip-instance_attributes-ip" name="ip" value="192.168.1.60"/>
        </instance_attributes>
        <operations>
          <op id="virtual_ip-monitor-interval-20s" interval="20s" name="monitor" timeout="20s"/>
          <op id="virtual_ip-start-interval-0s" interval="0s" name="start" timeout="20s"/>
          <op id="virtual_ip-stop-interval-0s" interval="0s" name="stop" timeout="20s"/>
        </operations>
      </primitive>
      <primitive class="systemd" id="docker-service" type="docker">
        <operations>
          <op id="docker-service-monitor-interval-20" interval="20" name="monitor" timeout="40s"/>
          <op id="docker-service-start-interval-0" interval="0" name="start" timeout="60s"/>
          <op id="docker-service-stop-interval-0" interval="0" name="stop" timeout="60s"/>
        </operations>
      </primitive>
    </resources>
    <constraints>
      <rsc_order first="p_drbd_drbdlh1-clone" first-action="promote" id="order-p_drbd_drbdlh1-clone-p_fs_drbd1-mandatory" then="p_fs_drbd1" then-action="start"/>
      <rsc_colocation id="colocation-p_fs_drbd1-p_drbd_drbdlh1-clone-INFINITY" rsc="p_fs_drbd1" score="INFINITY" with-rsc="p_drbd_drbdlh1-clone" with-rsc-role="Promoted"/>
      <rsc_order first="p_drbd_drbdlh1-clone" first-action="promote" id="order-p_drbd_drbdlh1-clone-virtual_ip-mandatory" then="virtual_ip" then-action="start"/>
      <rsc_colocation id="colocation-virtual_ip-p_drbd_drbdlh1-clone-INFINITY" rsc="virtual_ip" score="INFINITY" with-rsc="p_drbd_drbdlh1-clone" with-rsc-role="Promoted"/>
      <rsc_order first="p_drbd_drbdlh1-clone" first-action="promote" id="order-p_drbd_drbdlh1-clone-docker-service-mandatory" then="docker-service" then-action="start"/>
      <rsc_colocation id="colocation-docker-service-p_drbd_drbdlh1-clone-INFINITY" rsc="docker-service" score="INFINITY" with-rsc="p_drbd_drbdlh1-clone" with-rsc-role="Promoted"/>
    </constraints>
  </configuration>
  <status>
    <node_state id="1" uname="mirror1" in_ccm="true" crmd="online" crm-debug-origin="do_update_resource" join="member" expected="member">
      <lrm id="1">
        <lrm_resources>
          <lrm_resource id="virtual_ip" type="IPaddr2" class="ocf" provider="heartbeat">
            <lrm_rsc_op id="virtual_ip_last_0" operation_key="virtual_ip_start_0" operation="start" crm-debug-origin="do_update_resource" crm_feature_set="3.15.0" transition-key="34:11:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" transition-magic="0:0;34:11:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" exit-reason="" on_node="mirror1" call-id="30" rc-code="0" op-status="0" interval="0" last-rc-change="1680603114" exec-time="147" queue-time="0" op-digest="fc73825ad3d9995be3e3060f9a3a8317"/>
            <lrm_rsc_op id="virtual_ip_monitor_20000" operation_key="virtual_ip_monitor_20000" operation="monitor" crm-debug-origin="do_update_resource" crm_feature_set="3.15.0" transition-key="35:11:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" transition-magic="0:0;35:11:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" exit-reason="" on_node="mirror1" call-id="32" rc-code="0" op-status="0" interval="20000" last-rc-change="1680603114" exec-time="85" queue-time="0" op-digest="e8c4db8edfc5490dac694c7d0b461d3e"/>
          </lrm_resource>
          <lrm_resource id="p_drbd_drbdlh1" type="drbd" class="ocf" provider="linbit">
            <lrm_rsc_op id="p_drbd_drbdlh1_last_0" operation_key="p_drbd_drbdlh1_promote_0" operation="promote" crm-debug-origin="do_update_resource" crm_feature_set="3.15.0" transition-key="3:11:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" transition-magic="0:0;3:11:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" exit-reason="" on_node="mirror1" call-id="26" rc-code="0" op-status="0" interval="0" last-rc-change="1680603114" exec-time="110" queue-time="0" op-digest="783bbfdcdd1c0d3fd683d914becf9b3f" op-force-restart="  drbd_resource  " op-restart-digest="783bbfdcdd1c0d3fd683d914becf9b3f"/>
            <lrm_rsc_op id="p_drbd_drbdlh1_monitor_29000" operation_key="p_drbd_drbdlh1_monitor_29000" operation="monitor" crm-debug-origin="do_update_resource" crm_feature_set="3.15.0" transition-key="4:11:8:03f291a2-f9ec-4681-abae-c4e9d91b667d" transition-magic="0:8;4:11:8:03f291a2-f9ec-4681-abae-c4e9d91b667d" exit-reason="" on_node="mirror1" call-id="29" rc-code="8" op-status="0" interval="29000" last-rc-change="1680603114" exec-time="106" queue-time="0" op-digest="d1bc67a142953ef92537a5bbc66c41e0"/>
          </lrm_resource>
          <lrm_resource id="p_fs_drbd1" type="Filesystem" class="ocf" provider="heartbeat">
            <lrm_rsc_op id="p_fs_drbd1_last_0" operation_key="p_fs_drbd1_start_0" operation="start" crm-debug-origin="do_update_resource" crm_feature_set="3.15.0" transition-key="32:11:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" transition-magic="0:0;32:11:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" exit-reason="" on_node="mirror1" call-id="31" rc-code="0" op-status="0" interval="0" last-rc-change="1680603114" exec-time="342" queue-time="0" op-digest="5a63ac5ddaac3fa198c9532094ec6541"/>
            <lrm_rsc_op id="p_fs_drbd1_monitor_20000" operation_key="p_fs_drbd1_monitor_20000" operation="monitor" crm-debug-origin="do_update_resource" crm_feature_set="3.15.0" transition-key="33:11:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" transition-magic="0:0;33:11:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" exit-reason="" on_node="mirror1" call-id="33" rc-code="0" op-status="0" interval="20000" last-rc-change="1680603114" exec-time="100" queue-time="0" op-digest="595597c34a2936cb871ee5cab5db5261"/>
          </lrm_resource>
        </lrm_resources>
      </lrm>
      <transient_attributes id="1">
        <instance_attributes id="status-1">
          <nvpair id="status-1-master-p_drbd_drbdlh1" name="master-p_drbd_drbdlh1" value="10000"/>
        </instance_attributes>
      </transient_attributes>
    </node_state>
    <node_state id="2" uname="mirror2" crmd="online" crm-debug-origin="do_update_resource" in_ccm="true" expected="member" join="member">
      <transient_attributes id="2">
        <instance_attributes id="status-2">
          <nvpair id="status-2-master-p_drbd_drbdlh1" name="master-p_drbd_drbdlh1" value="10000"/>
        </instance_attributes>
      </transient_attributes>
      <lrm id="2">
        <lrm_resources>
          <lrm_resource id="virtual_ip" type="IPaddr2" class="ocf" provider="heartbeat">
            <lrm_rsc_op id="virtual_ip_last_0" operation_key="virtual_ip_stop_0" operation="stop" crm-debug-origin="do_update_resource" crm_feature_set="3.15.0" transition-key="40:10:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" transition-magic="0:0;40:10:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" exit-reason="" on_node="mirror2" call-id="76" rc-code="0" op-status="0" interval="0" last-rc-change="1680603082" exec-time="110" queue-time="0" op-digest="fc73825ad3d9995be3e3060f9a3a8317"/>
            <lrm_rsc_op id="virtual_ip_monitor_20000" operation_key="virtual_ip_monitor_20000" operation="monitor" crm-debug-origin="build_active_RAs" crm_feature_set="3.15.0" transition-key="33:8:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" transition-magic="0:0;33:8:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" exit-reason="" on_node="mirror2" call-id="67" rc-code="0" op-status="0" interval="20000" last-rc-change="1680602943" exec-time="90" queue-time="0" op-digest="e8c4db8edfc5490dac694c7d0b461d3e"/>
          </lrm_resource>
          <lrm_resource id="p_drbd_drbdlh1" type="drbd" class="ocf" provider="linbit">
            <lrm_rsc_op id="p_drbd_drbdlh1_last_0" operation_key="p_drbd_drbdlh1_demote_0" operation="demote" crm-debug-origin="do_update_resource" crm_feature_set="3.15.0" transition-key="10:10:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" transition-magic="0:0;10:10:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" exit-reason="" on_node="mirror2" call-id="80" rc-code="0" op-status="0" interval="0" last-rc-change="1680603084" exec-time="91" queue-time="0" op-digest="783bbfdcdd1c0d3fd683d914becf9b3f" op-force-restart="  drbd_resource  " op-restart-digest="783bbfdcdd1c0d3fd683d914becf9b3f"/>
            <lrm_rsc_op id="p_drbd_drbdlh1_monitor_31000" operation_key="p_drbd_drbdlh1_monitor_31000" operation="monitor" crm-debug-origin="do_update_resource" crm_feature_set="3.15.0" transition-key="7:11:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" transition-magic="0:0;7:11:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" exit-reason="" on_node="mirror2" call-id="84" rc-code="0" op-status="0" interval="31000" last-rc-change="1680603085" exec-time="102" queue-time="0" op-digest="d1bc67a142953ef92537a5bbc66c41e0"/>
          </lrm_resource>
          <lrm_resource id="p_fs_drbd1" type="Filesystem" class="ocf" provider="heartbeat">
            <lrm_rsc_op id="p_fs_drbd1_last_0" operation_key="p_fs_drbd1_stop_0" operation="stop" crm-debug-origin="do_update_resource" crm_feature_set="3.15.0" transition-key="37:10:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" transition-magic="0:0;37:10:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" exit-reason="" on_node="mirror2" call-id="74" rc-code="0" op-status="0" interval="0" last-rc-change="1680603082" exec-time="1627" queue-time="0" op-digest="5a63ac5ddaac3fa198c9532094ec6541"/>
            <lrm_rsc_op id="p_fs_drbd1_monitor_20000" operation_key="p_fs_drbd1_monitor_20000" operation="monitor" crm-debug-origin="build_active_RAs" crm_feature_set="3.15.0" transition-key="31:8:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" transition-magic="0:0;31:8:0:03f291a2-f9ec-4681-abae-c4e9d91b667d" exit-reason="" on_node="mirror2" call-id="68" rc-code="0" op-status="0" interval="20000" last-rc-change="1680602943" exec-time="101" queue-time="0" op-digest="595597c34a2936cb871ee5cab5db5261"/>
          </lrm_resource>
        </lrm_resources>
      </lrm>
    </node_state>
  </status>
</cib>

```
[Ledion Haderi](https://al.linkedin.com/in/ledion-haderi-00473968) :thumbsup:
